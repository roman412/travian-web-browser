﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravianWebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
//            this.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
//            WindowState = FormWindowState.Maximized;
            browser.Navigate("http://ts6.travian.ru/dorf1.php");
            browser.ScriptErrorsSuppressed = true;
        }
//-----------------------------Standart Functions (Common for all web browsers)---------------------------
        private void btnBack_Click(object sender, EventArgs e)
        {
            browser.GoBack();
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            browser.GoForward();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            browser.Navigate("http://ts6.travian.ru/dorf1.php");
        }
        private void AdressBar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                browser.Navigate(AdressBar.Text);
		browser.Navigate("www.youtube.com");
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            browser.Refresh();
        }
//--------------------------------------------------------------------------------------------------------
        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            AdressBar.Text = browser.Url.ToString();
            btnStart.Enabled = true;
        }


        private void toolStripButton2_Click(object sender, EventArgs e) //Authorization
        {
            HtmlElementCollection inputCollection = browser.Document.GetElementsByTagName("input"); //Get All 'input' on the page

            foreach (HtmlElement element in inputCollection) // Here and below type in the name of the user
            {
                if (element.Name == "name")
                {
                    element.Focus();
                    element.InnerText = "roman412";
                }
            }

            foreach (HtmlElement element in inputCollection) // Here and below type in the password of the user
            {
                if (element.Name == "password")
                {
                    element.Focus();
                    element.InnerText = "Trinitron1";
                }
            }

            HtmlElementCollection divCollection = browser.Document.GetElementsByTagName("div"); // Look for all 'div'
            

                foreach (HtmlElement divElement in divCollection) //Here and below press 'Enter' button
            {
                if (divElement.GetAttribute("className") == "button-content")
                {
                    divElement.InvokeMember("click");
                }
            }
        }

        private void browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            btnStart.Enabled = false;
        }
    }
}
